function scaleSpace = getScaleSpace2(im,scaleSpaceSize,initialScale,scalingFactor)
    [h,w] = size(im);
    scaleSpace = zeros(h,w,scaleSpaceSize);
    
    scale=initialScale;
    hsize = 2*ceil(scale*3)+1;
    filterResponse = filterImageWithLoG(im,hsize,scale);
    filterResponse = abs(filterResponse);
    scaleSpace(:,:,1) = filterResponse;
    for i=2:scaleSpaceSize
        scaledIm = imresize(im,1/((scalingFactor)^(i-1)));
        scaledFilterResponse = filterImageWithLoG(scaledIm,hsize,scale);
        scaledFilterResponse = abs(scaledFilterResponse);
        scaleSpace(:,:,i) = imresize(scaledFilterResponse, [h w], 'bicubic');
    end
end