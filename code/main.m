initialScale = 2;
scalingFactor=2;
scaleSpaceSize = 8;
threshold = 0.1;

im = imread('../data/butterfly.jpg');
disp('butterfly')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/einstein.jpg');
disp('einstein')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/fishes.jpg');
disp('fishes')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/sunflowers.jpg');
disp('sunflowers')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/Netherlands.jpg');
disp('Netherlands')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/leopard.jpg');
disp('leopard')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/nautilis.jpg');
disp('nautilis')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);

im = imread('../data/ocean.jpg');
disp('ocean')
blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold);


