% Not used here. Used in previously tried out implementation.
function  blobs = setRadiusValues(nonMaxSuppressed3D, scaleArray, threshold)
    [h, w, s] = size(nonMaxSuppressed3D);
    nonMaxSuppressedSpace = zeros(h,w,s);
    for i=1:s
        scale = scaleArray(i);
        radius = sqrt(2)*scale;
        nonMaxSuppressed = nonMaxSuppressed3D(:,:,i);
        nonMaxSuppressed(nonMaxSuppressed < threshold) = 0;
        nonMaxSuppressed(nonMaxSuppressed > 0) = radius;
        nonMaxSuppressedSpace(:,:,i) = nonMaxSuppressed;
    end
    blobs = max(nonMaxSuppressedSpace,[],3);
end