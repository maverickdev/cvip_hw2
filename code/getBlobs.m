function blobs = getBlobs(scaleSpace,initialScale,scalingFactor,threshold)
    [h,w,scaleSpaceSize] = size(scaleSpace); 
    
%     Commented code part of previous implementation
%     ordFiltVals3D = zeros(h,w,scaleSpaceSize);

%     scale=initialScale;
%     scaleArray = zeros(scaleSpaceSize,1);
%     for i=1:scaleSpaceSize
% %         radiusCeil = ceil(sqrt(2)*scale);
%         ordFiltVals = ordfilt2(scaleSpace(:,:,i), 3*3, ones(3));
% %       ordFiltVals = ordfilt2(scaleSpace(:,:,i), radiusCeil*radiusCeil, ones(radiusCeil));
%         ordFiltVals3D(:,:,i) = ordFiltVals;
%         scaleArray(i) = scale;
%         scale = ceil(scale*scalingFactor);
%     end
% 
%     ordFiltMaxVals = max(ordFiltVals3D,[],3);
%     ordFiltMaxVals3D = repmat(ordFiltMaxVals, 1,1,scaleSpaceSize);
%     nonMaxSuppressed3D = scaleSpace .* (ordFiltMaxVals3D == scaleSpace);
%     
%     blobs = setRadiusValues(nonMaxSuppressed3D,scaleArray, threshold);
    
    [maxImage,idx] = max(scaleSpace,[],3);
    ordFiltVals = ordfilt2(maxImage, 3*3, ones(3));
    nonMaxSuppressed = maxImage .* (ordFiltVals == maxImage);
    nonMaxSuppressed(nonMaxSuppressed<threshold) = 0;
    
    scaleRadius = zeros(scaleSpaceSize,1);
    scale = initialScale;
    for i=1:scaleSpaceSize
        scaleRadius(i) = scale * sqrt(2);
        scale = scale*scalingFactor;
    end
    
    blobs = (nonMaxSuppressed >0) .* scaleRadius(idx);
    
end