function blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold)
    im = rgb2gray(im);
    im = im2double(im);
    tic
    scaleSpace = getScaleSpace(im,scaleSpaceSize,initialScale,scalingFactor);
    toc
    tic
    scaleSpace2 = getScaleSpace2(im,scaleSpaceSize,initialScale,scalingFactor);
    toc
    
    blobs = getBlobs(scaleSpace,initialScale,scalingFactor,threshold);
    blobs2 = getBlobs(scaleSpace2,initialScale,scalingFactor,threshold);

    displayBlobs(im,blobs,1);
    displayBlobs(im,blobs2,2);
    
end

function displayBlobs(im,blobs,count)
    [cx,cy,rads]=find(blobs);
    figure(count);
    show_all_circles(im,cy,cx,rads);
end