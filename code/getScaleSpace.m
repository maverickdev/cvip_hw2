function scaleSpace = getScaleSpace(im,scaleSpaceSize,initialScale,scalingFactor)
    [h,w] = size(im);
    scaleSpace = zeros(h,w,scaleSpaceSize);
    scale=initialScale;
    scaleArray = zeros(scaleSpaceSize);
    for i=1:scaleSpaceSize
        hsize = 2*ceil(scale*3)+1;
        scaleArray(i) = scale;
        filterResponse = filterImageWithLoG(im,hsize,scale);
        filterResponse = abs(filterResponse);
        scaleSpace(:,:,i) = filterResponse;
        scale = scale*scalingFactor;
    end

end